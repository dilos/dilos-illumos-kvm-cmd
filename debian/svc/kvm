#!/bin/sh
#
# Copyright (c) 2012-2018, DilOS.
#
# Permission is hereby granted, free of charge, to any person obtaining a  copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the  rights
# to use, copy, modify, merge, publish,  distribute,  sublicense,  and/or  sell
# copies of the Software, and  to  permit  persons  to  whom  the  Software  is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall  be  included  in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY  KIND,  EXPRESS  OR
# IMPLIED, INCLUDING BUT NOT LIMITED  TO  THE  WARRANTIES  OF  MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT  SHALL  THE
# AUTHORS OR COPYRIGHT HOLDERS BE  LIABLE  FOR  ANY  CLAIM,  DAMAGES  OR  OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

# Verion 0.2

#
# Standard prolog
#
. /lib/svc/share/smf_include.sh

if [ -z $SMF_FMRI ]; then
	echo "SMF framework variables are not initialized."
	exit $SMF_EXIT_ERR
fi

KVM=/usr/bin/kvm
KVMDIR=/var/run/kvm
mkdir -p -m 0700 ${KVMDIR}

INSTANCE=`echo $SMF_FMRI|sed 's/.*://g'`
VMDIR=${KVMDIR}/$INSTANCE
PIDFILE=${VMDIR}/pid
CTLFILE=${VMDIR}/ctl
VNCFILE=${VMDIR}/vnc
CONSOLE=${VMDIR}/console
SMBIOS="type=1,manufacturer=DilOS,product=KVM,version=2.0.1,sku=001,family=VirtualMachine"

DEFAULT_HVM_CPU=qemu64
DEFAULT_HVM_SMP=1
DEFAULT_HVM_CORES=1
DEFAULT_HVM_THREADS=1
DEFAULT_HVM_SOCKETS=1
DEFAULT_HVM_MEM=512m
DEFAULT_HVM_VGA=std
DEFAULT_VNIC_MODEL=virtio
DEFAULT_DISK_MODEL=virtio
DEFAULT_SERIAL_MODE=server
DEFAULT_SERIAL_OPTION=nowait


get_mac()
{
	local vnic=$1
	local mac=`dladm show-vnic -p -o macaddress $vnic`
	IFS=:
	set $mac
	printf "%02x:%02x:%02x:%02x:%02x:%02x\n" 0x$1 0x$2 0x$3 0x$4 0x$5 0x$6
}


# Returns value from a property or its default value
getprop()
{
	local val=`svcprop -c -p $1 $SMF_FMRI 2>/dev/null`

	if [ $? -ne 0 ] || [ -z "$val" ]
	then
		val=`svcprop -c -p $1 svc:/system/kvm:default 2>/dev/null`
		if [ $? -ne 0 ] || [ -z "$val" ]
		then
			val=$2
		fi
	fi

	echo ${val}
}


get_hvm()
{
	local cpu=`getprop hvm/cpu ${DEFAULT_HVM_CPU}`
	local smp=`getprop hvm/smp ${DEFAULT_HVM_SMP}`
	local cores=`getprop hvm/cores ${DEFAULT_HVM_CORES}`
	local threads=`getprop hvm/threads ${DEFAULT_HVM_THREADS}`
	local sockets=`getprop hvm/sockets ${DEFAULT_HVM_SOCKETS}`
	local mem=`getprop hvm/mem ${DEFAULT_HVM_MEM}`
	local vga=`getprop hvm/vga ${DEFAULT_HVM_VGA}`
	local args="-cpu ${cpu}"

	if [ ${smp} -gt 1 ] || [ ${cores} -gt 1 ] || [ ${threads} -gt 1 ] || [ ${sockets} -gt 1 ]; then
		args="${args} -smp ${smp}"
		if [ ${cores} -gt 1 ]; then
			args="${args},cores=${cores}"
		fi
		if [ ${threads} -gt 1 ]; then
			args="${args},threads=${threads}"
		fi
		if [ ${sockets} -gt 1 ]; then
			args="${args},sockets=${sockets}"
		fi
	fi

	if [ ! -z ${mem} ]; then
		args="${args} -m ${mem}"
	fi

	if [ ! -z ${vga} ] && [ "${vga}" != "std" ]; then
		args="${args} -vga ${vga}"
	fi

	echo "${args}"
}


get_vnic_name()
{
	local i=0
	local name
	local file=/tmp/kvm.$$

	dladm show-vnic -p -o link|grep '^kvm'|sort >${file}
	while read vnic; do
		name="kvm${i}"
		if [ "$name" != "${vnic}" ]; then
			break;
		fi
		i=$((i+1))
	done <${file}
	rm -f ${file}
	echo "kvm${i}"
}


get_vnic()
{
	local vnic=`getprop $1/name`
	local over=`getprop $1/link`
	local model=`getprop $1/model ${DEFAULT_VNIC_MODEL}`
	local mac=`getprop $1/mac`
	local vlan=`getprop $1/vlan 0`
	local ARGS
	local DEVICE=""
	local tmp

	if [ -z "$over" ]; then
		echo "No parent link for ${vnic}"
		exit 1
	fi
	if [ -z ${vnic} ]; then
		vnic=`get_vnic_name`
		svccfg -s ${SMF_FMRI} setprop $1/name = astring: ${vnic}
	fi
	dladm show-vnic ${vnic} >/dev/null 2>&1
	if [ $? -ne 0 ]; then
		if [ ! -z "${mac}" ]; then
			ARGS="-m ${mac}"
		fi
		if [ ! -z "${vlan}" ] && [ ${vlan} -gt 0 ]; then
			ARGS="${ARGS} -v ${vlan}"
		fi
		dladm create-vnic -l ${over} ${ARGS} ${vnic}
		if [ $? -ne 0 ]; then
			exit 1
		fi
		if [ -z ${mac} ]; then
			mac=`get_mac ${vnic}`
			svccfg -s ${SMF_FMRI} setprop $1/mac = astring: ${mac}
		fi
	fi
	vndadm list ${vnic} >/dev/null 2>&1
	if [ $? -ne 0 ]; then
		devfsadm -i vnd
		vndadm create ${vnic}
	fi

	case "${model}" in
	virtio)
		DEVICE="-device virtio-net-pci,mac=${mac},tx=timer,x-txtimer=200000,x-txburst=128,vlan=${2}"
		;;
	*)
		;;
	esac

	ARGS="vnic,vlan=${2},ifname=${vnic},macaddr=${mac}"
	tmp=`getprop $1/ip`
	if [ ! -z "${tmp}" ]; then
		ARGS="${ARGS},ip=${tmp}"
	fi
	tmp=`getprop $1/netmask`
	if [ ! -z "${tmp}" ]; then
		ARGS="${ARGS},netmask=${tmp}"
	fi
	tmp=`getprop $1/gateway`
	if [ ! -z "${tmp}" ]; then
		ARGS="${ARGS},gateway_ip=${tmp}"
	fi
	tmp=`getprop $1/hostname`
	if [ ! -z "${tmp}" ]; then
		ARGS="${ARGS},hostname=${tmp}"
	fi
	tmp=`getprop $1/dns0`
	if [ ! -z "${tmp}" ]; then
		ARGS="${ARGS},dns_ip0=${tmp}"
	fi
	tmp=`getprop $1/dns1`
	if [ ! -z "${tmp}" ]; then
		ARGS="${ARGS},dns_ip1=${tmp}"
	fi
	echo "${DEVICE} -net ${ARGS}"
}


destroy_vnic()
{
	local vnic=`getprop $1/name`

	vndadm list ${vnic} >/dev/null 2>&1
	if [ $? -eq 0 ]; then
		vndadm destroy ${vnic}
	fi
	dladm show-vnic ${vnic} >/dev/null 2>&1
	if [ $? -eq 0 ]; then
		dladm delete-vnic ${vnic}
	fi
}


get_net()
{
	local i=0
	local file=/tmp/net.$$

	svcprop -c -p net $SMF_FMRI >${file}
	while read dummy dummy prop; do
		get_vnic ${prop} ${i}
		i=$((i+1))
	done <${file}
	rm -f ${file}
}


destroy_net()
{
	local i=0
	local file=/tmp/net.$$

	svcprop -c -p net $SMF_FMRI >${file}
	while read dummy dummy prop; do
		destroy_vnic ${prop} ${i}
		i=$((i+1))
	done <${file}
	rm -f ${file}
	devfsadm -i vnd
}


get_zvol()
{
	local zvol=`getprop $1/zvol`
	local model=`getprop $1/model ${DEFAULT_DISK_MODEL}`
	local serial=`getprop $1/serial ${RANDOM}`
	local boot=`getprop $1/boot`
	local dev="/dev/zvol/rdsk/${zvol}"
	local ARGS

	if [ ! -c ${dev} ]; then
		if [ -f ${dev} ]; then
			echo "${dev} is not special zvol device"
		else
			echo "${dev} does not exist"
		fi
		exit 1
	fi


	ARGS="file=${dev},if=${model},index=${2},media=disk"
	if [ ! -z ${serial} ]; then
		ARGS="${ARGS},serial=${serial}"
	fi
	if [ ! -z ${boot} ]; then
		case "${boot}" in
		on|off)
			ARGS="${ARGS},boot=${boot}"
			;;
		*)
			echo "Unknown boot argument ${boot}"
			;;
		esac
	fi

	echo "-drive ${ARGS}"
}


get_disk()
{
	local i=0
	local file=/tmp/disk.$$

	svcprop -c -p disk $SMF_FMRI >${file}
	while read dummy dummy prop; do
		get_zvol ${prop} ${i}
		i=$((i+1))
	done <${file}
	rm -f ${file}
}


get_serial()
{
	local type=`getprop serial/type`
	local mode=`getprop serial/mode ${DEFAULT_SERIAL_MODE}`
	local option=`getprop serial/option ${DEFAULT_SERIAL_OPTION}`
	local bind=`getprop serial/bind`
	local path=`getprop serial/path ${CONSOLE}`
	local target=`getprop serial/target`
	local ARGS="-serial chardev:serial0 -chardev"
	local dev;

	case "${type}" in
	udp)
		if [ ! -z "${target}" ]; then
			dev="udp:${target}"
			if [ ! -z "${bind}" ]; then
				dev="${dev}@${bind}"
			fi
		fi
		;;
	tcp)
		if [ ! -z "${bind}" ]; then
			dev="tcp:${bind}"
		elif [ ! -z "${target}" ]; then
			dev="tcp:${target}"
		fi
		if [ ! -z "${dev}" ]; then
			ARGS="-serial";
			[ "${mode}" = "server" ] && dev="$dev,${mode}"
			[ ! -z "${option}" ] && dev="$dev,${option}"
		fi
		;;
	telnet)
		if [ ! -z "${bind}" ]; then
			dev="telnet:${bind}"
		elif [ ! -z "${target}" ]; then
			dev="telnet:${target}"
		fi
		if [ ! -z "${dev}" ]; then
			ARGS="-serial";
			[ "${mode}" = "server" ] && dev="$dev,${mode}"
			[ ! -z "${option}" ] && dev="$dev,${option}"
		fi
		;;
	unix)
		if [ ! -z "${path}" ]; then
			dev="socket,path=${path}"
			[ "${mode}" = "server" ] && dev="$dev,${mode}"
			[ ! -z "${option}" ] && dev="$dev,${option}"
		fi
		;;
	esac

	if [ -z "${dev}" ]; then
		dev="socket,path=${path}"
		[ "${mode}" = "server" ] && dev="$dev,${mode}"
		[ ! -z "${option}" ] && dev="$dev,${option}"
	fi

	echo "${ARGS} ${dev},id=serial0"
}


get_boot()
{
	local order=`getprop boot/order`
	local once=`getprop boot/once`
	local menu=`getprop boot/menu`
	local boot=""

	if [ ! -z "${order}" ]; then
		boot="order=${order}"
	fi
	if [ ! -z "${once}" ]; then
		[ ! -z "${boot}" ] && boot="${boot},"
		boot="once=${once}"
	fi
	if [ ! -z "${menu}" ]; then
		case "${menu}" in
		on|off)
			[ ! -z "${boot}" ] && boot="${boot},"
			boot="menu=${menu}"
			;;
		esac
	fi

	if [ ! -z "${boot}" ]; then
		echo "-boot ${boot}"
	fi
}


kvm_start()
{
	local ARGS="-daemonize -nographic\
 -pidfile ${PIDFILE}\
 -monitor unix:${CTLFILE},server,nowait\
 -vnc unix:${VNCFILE}\
 -smbios ${SMBIOS}\
 `get_boot`
 `get_serial`\
 `get_hvm`\
 `get_disk`\
 `get_net`\
"
	mkdir -p -m 0700 ${VMDIR}
	$KVM $ARGS
}


kvm_stop()
{
	local timeout;
	local i
	local to

	if [ -S ${CTLFILE} ] && [ -f ${PIDFILE} ]
	then
		echo 'system_powerdown'|/bin/nc -U ${CTLFILE}
		if [ $? -eq 0 ]; then
			timeout=`svcprop -c -p stop/timeout_seconds $SMF_FMRI`
		fi
		PID=`cat ${PIDFILE}`
		to=$((timeout/2))
		for i in {1..${to}}; do
			kill -0 $PID >/dev/null 2>&1 || break
			sleep 1
		done
		kill -0 $PID >/dev/null 2>&1
		if [ $? -eq 0 ]
		then
			echo 'quit'|/bin/nc -U ${CTLFILE}
			if [ $? -eq 0 ]; then
				for i in {1..${to}}; do
					kill -0 $PID >/dev/null 2>&1 || break
					sleep 1
				done
				kill -0 $PID >/dev/null 2>&1
				if [ $? -eq 0 ]
				then
					kill -2 $PID
				fi
			fi
		fi
	fi
	destroy_net
	rm -rf ${VMDIR}
}

case "$1" in
	start)
		kvm_start
		;;
	stop)
		kvm_stop
		;;
	*)
		echo "$0 (start | stop)"
		;;
esac

exit $SMF_EXIT_OK
