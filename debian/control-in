Source: qemu
Section: otherosfs
Priority: optional
Maintainer: DilOS Team
Build-Depends: debhelper (>= 9),
# In comments below we also specify (system-specific) arguments
# to qemu's configure script, -- optional features which depend
# on build-dependencies.
# pc-bios/*.dts => *.dtb (PPC firmware)
 device-tree-compiler,
 texinfo,
 python:any,
# iasl (from acpica-tools) is used only in a single test these days, not for building
# acpica-tools,
# --enable-capstone=system
 libcapstone-dev,
# --enable-linux-aio	linux-*
 libaio-dev [linux-any],
# --audio-drv-list=pa,alsa,oss	linux-*
# --audio-drv-list=pa,oss	kfreebsd-*
 libpulse-dev [linux-any],
 libasound2-dev [linux-any],
# for virtfs
# --enable-attr
 libattr1-dev [linux-any],
# --enable-bluez	linux-*
 libbluetooth-dev [linux-any],
# --enable-brlapi
 libbrlapi-dev [linux-any],,
# --enable-virtfs	linux-*
# needed for virtfs
 libcap-dev [linux-any],
# --enable-cap-ng	linux-*
 libcap-ng-dev [linux-any],
# --enable-curl
 libcurl4-gnutls-dev,
# --enable-fdt
 libfdt-dev,
# --enable-gnutls
 gnutls-dev,
# --enable-gtk --with-gtkabi=3.0 --enable-vte
 libgtk-3-dev,
 libvte-2.91-dev [linux-any],
# --enable-libiscsi
 libiscsi-dev (>> 1.9.0~) [linux-any],
# --enable-curses
 libncursesw5-dev,
:debian:# --enable-libnfs
:debian: libnfs-dev (>> 1.9.3),
# --enable-numa	i386|amd64|ia64|mips|mipsel|powerpc|powerpcspe|x32|ppc64|ppc64el|arm64|sparc
 libnuma-dev   [i386 amd64 ia64 mips mipsel mips64 mips64el powerpc powerpcspe x32 ppc64 ppc64el arm64 sparc],
# --enable-smartcard
 libcacard-dev,
 libpixman-1-dev,
# --enable-rbd		linux-*
 librados-dev [linux-any], librbd-dev [linux-any],
# glusterfs is debian-only since ubuntu/glusterfs is in universe (MIR LP: #1274247)
:debian:# --enable-glusterfs
:debian: glusterfs-common,
# --enable-vnc-sasl
 libsasl2-dev,
# --disable-sdl --with-sdlabi=2.0
# libsdl1.2-dev,
# --enable-seccomp	linux-*
 libseccomp-dev (>= 2.3.0) [linux-any],
# --enable-spice	linux-amd64|linux-i386
 libspice-server-dev (>= 0.12.2~) [linux-amd64 linux-i386],
 libspice-protocol-dev (>= 0.12.3~) [linux-amd64 linux-i386],
# --enable-libusb	linux-*
 libusb-1.0-0-dev (>= 2:1.0.13~) [linux-any],
# --enable-usb-redir	linux-*
 libusbredirparser-dev (>= 0.6~) [linux-any],
# libssh2 is debian-only since ubuntu/libssh2 is in universe
:debian:# --enable-libssh2
:debian: libssh2-1-dev,
# vde is debian-only since ubuntu/vde2 is in universe
:debian:# --enable-vde
:debian: libvdeplug-dev,
# --enable-xen	linux-amd64|linux-i386
 libxen-dev [linux-amd64 linux-i386],
## always enabled: --enable-uuid
 uuid-dev,
# --enable-xfsctl	linux-*
 xfslibs-dev [linux-any],
# always needed
 zlib1g-dev,
# other optional features we enable
# --enable-vnc
# --enable-vnc-jpeg
 libjpeg-dev,
# --enable-vnc-png
 libpng-dev,
# --enable-kvm		linux-*
# --enable-vhost-net	linux-*	# is it really linux-specific?
##--enable-lzo todo
##--enable-netmap todo bsd
##--enable-quorum todo needs gcrypt
##--enable-xen-pci-passthrough todo
# for dilos
# libvnd-dev,
 driver-kvm-dev,
 developer-dtrace
Build-Conflicts: oss4-dev
Standards-Version: 3.9.8
Homepage: http://www.qemu.org/
:debian:Vcs-Browser: https://salsa.debian.org/qemu-team/qemu
:ubuntu:Vcs-Browser: https://salsa.debian.org/qemu-team/qemu/commits/ubuntu-utopic
:ubuntu:XS-Debian-Vcs-Browser: https://salsa.debian.org/qemu-team/qemu
:debian:Vcs-Git: git@salsa.debian.org:qemu-team/qemu.git
:ubuntu:Vcs-Git: git@salsa.debian.org:qemu-team/qemu.git -b ubuntu-utopic
:ubuntu:XS-Debian-Vcs-Git: git@salsa.debian.org:qemu-team/qemu.git
:dilos:Vcs-Git: https://bitbucket.org/dilos/dilos-illumos-kvm-cmd/commits/all

Package: qemu
Architecture: solaris-i386
Multi-Arch: foreign
Section: oldlibs
Depends: ${misc:Depends}, qemu-system
Description: fast processor emulator, dummy package
 QEMU is a fast processor emulator. Once upon a time there was only one
 package named `qemu', with all functionality included. These days, qemu
 become large and has been split into numerous packages. Different packages
 provides entirely different services, and it is very unlikely one will
 need all of them together. So current `qemu' package makes no sense anymore,
 and is becoming a dummy package.
 .
 If you want full system emulation of some architecture, install one or more
 of qemu-system-ARCH packages. If you want user-mode emulation, install
 qemu-user pr qemu-user-static package. If you need utilities, use qemu-utils
 package.
 .
 This package can safely be removed.

Package: qemu-system
Architecture: solaris-i386
Multi-Arch: foreign
Depends: ${misc:Depends}, qemu-system-x86
Description: QEMU full system emulation binaries
 QEMU is a fast processor emulator: currently the package supports
 ARM, CRIS, i386, M68k (ColdFire), MicroBlaze, MIPS, PowerPC, SH4,
 SPARC and x86-64 emulation. By using dynamic translation it achieves
 reasonable speed while being easy to port on new host CPUs.
 .
 This metapackage provides the full system emulation binaries for all supported
 targets, by depending on all per-architecture system emulation packages which
 QEMU supports.

Package: qemu-system-x86
Architecture: solaris-i386
Multi-Arch: foreign
Depends: ${shlibs:Depends}, ${misc:Depends}
Recommends: qemu-utils,
Suggests: samba
Description: QEMU full system emulation binaries (x86)
 QEMU is a fast processor emulator: currently the package supports
 i386 and x86-64 emulation. By using dynamic translation it achieves
 reasonable speed while being easy to port on new host CPUs.
 .
 This package provides the full system emulation binaries to emulate
 the following x86 hardware: x86_64.
 .
 In system emulation mode QEMU emulates a full system, including a processor
 and various peripherals.  It enables easier testing and debugging of system
 code.  It can also be used to provide virtual hosting of several virtual
 machines on a single server.
 .
 On x86 host hardware this package also enables KVM kernel virtual machine
 usage on systems which supports it.

Package: qemu-utils
Architecture: solaris-i386
Multi-Arch: foreign
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: QEMU utilities
 QEMU is a fast processor emulator: currently the package supports
 ARM, CRIS, i386, M68k (ColdFire), MicroBlaze, MIPS, PowerPC, SH4,
 SPARC and x86-64 emulation. By using dynamic translation it achieves
 reasonable speed while being easy to port on new host CPUs.
 .
 This package provides QEMU related utilities:
  * qemu-img: QEMU disk image utility
  * qemu-io:  QEMU disk exerciser
  * qemu-nbd: QEMU disk network block device server

Package: kvm
Architecture: solaris-i386
Multi-Arch: foreign
Depends: qemu-system, driver-kvm, netcat-openbsd
Description: QEMU kvm meta package
 QEMU is a fast processor emulator
